# idnmon-website

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### 项目搭建

1.vue ui图形化界面创建项目

2.gitlab 新建仓库https://gitlab.com/mr.six/idnmon-website

（1）初次使用gitlab在user setting中设置SSH KEY(可以直接搜索SSH KEY)

(2)连接远程仓库
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
