import Vue from 'vue';
import VueRouter from 'vue-router';
import IdnMon from '@/views/IdnMon.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'IdnMon',
    component: IdnMon,
  },
  {
    path: '/languageDetec',
    name: 'languageDetec',
    component: () => import('../views/LanguageDetect.vue'),
    // component: () =>
    //   import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
  },
  {
    path: '/homograph',
    name: 'homograph',
    component: () => import('@/views/Homograph.vue')
  }
];

const router = new VueRouter({
  routes,
});

export default router;
